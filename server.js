require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = process.env.PORT || 3000;
const apikey = 'apiKey='+process.env.APIKEY;
const URL_BASE = '/techu/v1/';
const URL_BASE_M = '/techu/v2/';
const usersfile = require('./user.json');
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu14db/collections/';



//npm run dev:  contiene nodemon
//npm install descarga dependencies definidas en el package.json

//Realiza el parseo de los parámetros dedel body del request
app.use(body_parser.json());

app.listen(port, function(){
    console.log("Node escuchando en el puerto : " + port);
});

// Operación GET (Collection)
app.get(URL_BASE_M + "users", function(req,res){
  //Send: última linea de código, para finalizar como respuesta al cliente.
  let http_client = request_json.createClient(URL_mLab);
  let field_param = 'f={"_id":0}&';
  console.log("Cliente mlab creado");
  http_client.get('user?'+ field_param + apikey,
  function(err, respuestaMLab, body){
    console.log('Error: ' + err);
    console.log('Respuesta MLab: ' + respuestaMLab);
    console.log('Body: ' + body)
    var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
  });
  //response.status(200).send("Cliente mlab creado");

  app.get(URL_BASE_M + "users/:id", function(req,res){
    //Send: última linea de código, para finalizar como respuesta al cliente.
    let id = req.params.id;
    let queryString = 'q={"id_user":' + id + '}&';
    let field_param = 'f={"_id":0}&';
    let http_client = request_json.createClient(URL_mLab);
    console.log(queryString+ "  " + field_param);
    http_client.get('user?'+ queryString + field_param + apikey,

    function(error,res_mlab, body){
      var response = {};
      if(error) {
          response = {"msg" : "Error  en la petición a mLab."}
          res.status(500);
      } else {
        if(body.length > 0){
          response = body;
        } else {
          response = {'msg': 'Usuario no encontrado'}
          res.status(400);
        }
      }
      res.send(response);
    });

  });

  app.get(URL_BASE_M + "users/:id/accounts", function(req,res){
    let id = req.params.id;
    let queryString = 'q={"id_user":' + id + '}&';
    let field_param = 'f={"account":1,"_id":0}&';
    console.log(queryString+ "  " + field_param);
    let http_client = request_json.createClient(URL_mLab);
    http_client.get('user?'+ queryString + field_param + apikey,
    function(error, res_mlab, body){
      var response = {};
      if(error) {
          response = {"msg" : "Error  en la petición a mLab."}
          res.status(500);
      } else {
        if(body.length > 0){
          response = body;
        } else {
          response = {'msg': 'Usuario no encontrado'}
          res.status(400);
        }
      }
      res.send(response);
    });
  });

  //POST of user
app.post(URL_BASE_M + "users",
 function(req, res) {
   var newID=0;
  var  clienteMlab = request_json.createClient(URL_mLab);
  clienteMlab.get('user?'+ apikey ,
  function(error, respuestaMLab , body) {
      newID=body.length+1;
      console.log("newID:" + newID);
      var newUser = {
        "id_user" : newID,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };
      clienteMlab.post(URL_mLab + "user?" + apikey, newUser ,
       function(error, respuestaMLab, body) {
        res.send(body);
     });
  });
});

app.put(URL_BASE_M + 'users/:id',
function(req, res) {
var  clienteMlab = request_json.createClient(URL_mLab);
var newID=0;
 clienteMlab.get('user?'+ apikey ,
 function(error, respuestaMLab , body) {
   console.log("newID:" + newID);
     newID=body.length+1;
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     clienteMlab.put(URL_mLab + 'user?q={"id_user": ' + newID + '}&' + apikey, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body);
        //body.n ==1 se logro realizar el update
       res.send(body);
     });
 });
});

//DELETE user with id
app.delete(URL_BASE_M + "users/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: "+req.params.id);
    var id=req.params.id;
    var queryStringID='q={"id_user":' + id + '}&';
    console.log(URL_mLab + 'user?' + queryStringID + apikey);
    var httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' +  queryStringID + apikey,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ respuesta);
        httpClient.delete(URL_mLab + "user/"+ respuesta._id.$oid +'?'+ apikey,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });

  //Method POST login
app.post(URL_BASE_M + "login",
  function (req, res){
    let email = req.body.email;
    let pass = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFilter = 'l=1&';
    let clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryString + limFilter + apikey,
      function(error, respuestaMLab, body) {
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"id_user": ' + body[0].id_user + '}&' + apikey, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id_user, 'name':body[0].first_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
          }
          else {
            res.status(404).send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//Method POST logout
app.post(URL_BASE_M + "logout",
  function(req, res) {
    var email = req.body.email;
    var queryString = 'q={"email":"' + email + '","logged":true}&';
    console.log(queryString);
    var  clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryString + apikey,
      function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if(!error) {
          if (respuesta != undefined) {  // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            clienteMlab.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikey, JSON.parse(logout),
            //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Logout correcto', 'user':respuesta.email});
                // If bodyPut.n == 1, put de mLab correcto
              });
            } else {
                res.status(404).send({"msg":"Logout failed!"});
            }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});




//Operación GEt by ID

app.get(URL_BASE + "users/:id", function(request, response){
  console.log(request.params.id);
  let pos=request.params.id-1;
  let respuesta =
    (usersfile[pos] == undefined) ? {"msg": "Usuario no existe"} : usersfile[pos];
  response.send(respuesta);

});

// Petición GET con Query String (req.query)
app.get(URL_BASE + 'users',
  function(req, res) {
    console.log("GET con query string.");
    console.log(req.query.id);
    console.log(req.query.country);
    res.send(usersFile[pos - 1]);
    respuesta.send({"msg" : "GET con query string"});
});

app.post(URL_BASE + "users", function(request, response){
  console.log("Desde método POST");
  let tam = usersfile.length;
  let new_user = {
    "id_user": tam + 1,
    "first_name": request.body.first_name,
    "last_name": request.body.last_name,
    "email": request.body.email,
    "password": request.body.password
  }
  console.log(new_user);
  usersfile.push(new_user);
  response.send({"msg": "Usuario creado correctamente"});
});


app.put(URL_BASE + "users/:id", function(request, response){
  console.log("Desde método PUT");
  let pos=request.params.id-1;
  let upd_user = {
    "id_user": request.params.id,
    "first_name": request.body.first_name,
    "last_name": request.body.last_name,
    "email": request.body.email,
    "password": request.body.password
  }
  console.log(upd_user);
  usersfile[pos] = upd_user;
  response.send({"msg": "Usuario con id : " + request.params.id + " actualizado correctamente"});
});

//optimizado
app.put(URL_BASE + "actualizar/:id", function(request, response){
  let pos = usersfile.findIndex(v => v.id_user==request.params.id);
  if(pos==-1){
    response.send({"msg": "No se encontró el Usuario con id : " + pos})
  }
  let upd_user = {
    "id_user": request.params.id,
    "first_name": request.body.first_name,
    "last_name": request.body.last_name,
    "email": request.body.email,
    "password": request.body.password
  }
  usersfile[pos] = upd_user;
  response.send({"msg": "Usuario con id : " + pos + " actualizado correctamente"});
});

app.put(URL_BASE + 'ezequiel/:id',
   function(req, res){
     console.log("PUT /techu/v1/users/:id");
     let idBuscar = req.params.id;
     let updateUser = req.body;
     for(i = 0; i < usersFile.length; i++) {
       console.log(usersFile[i].id);
       if(usersFile[i].id == idBuscar) {
	     usersFile[i] = updateUser;
         res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
       }
     }
     res.send({"msg" : "Usuario no encontrado.", updateUser});
   });

app.delete(URL_BASE + "users/:id", function(request, response){
  let pos = usersfile.findIndex(v => v.id_user==request.params.id);
  if(pos==-1){
    response.status(404).send({"msg": "Usuario con id : " + request.params.id + " no existe para su eliminación."})
  }
  usersfile.splice(request.params.id-1,1);
  response.status(200).send({"msg": "Usuario con id : " + request.params.id + " fue eliminado."});
});

// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /techu/v1/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersfile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersfile);
          console.log("Login correcto!");
          response.send({"msg" : "Login correcto.", "idUsuario" : us.id_user, "logged" : "true"});
        } else {
          console.log("Login incorrecto.");
          response.send({"msg" : "Login incorrecto."});
        }
      }
    }
    response.send({"msg" : "Login incorrecto."});
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   });
 }


 app.post(URL_BASE + 'logout',
  function(request, response) {
    console.log("POST /techu/v1/logout");
    var userId = request.body.id;
    for(us of usersfile) {
      if(us.id_user == userId) {
        if(us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersfile);
          console.log("Logout correcto!");
          response.send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
        } else {
          console.log("Logout incorrecto.");
          response.send({"msg" : "Logout incorrecto."});
        }
      }  us.logged = true
    }
});

app.get(URL_BASE + "total_users",
(request, response)=>{
  response.send({"num_usuarios": usersfile.length});
});
